# Toggle Declined Events for Google Calendar

Quickly make your declined events visible/hidden on Google Calendar without having to drill into the Settings menu, scroll to the setting, select, and save. Saves a lot of clicking and scrolling time!

This extension simply adds a button to the left of the "More" menu which will toggle the declined events setting on/off. It does this by simulating the clicks you would use, so there is a little flicker as it switches screens. This is by design.

Last updated 2016-10-08

Extension icon from Edward Boatman on The Noun Project

# Instructions

* Clone repository
* Open chrome://extensions
* Make sure "Developer mode" is checked
* Click button "Load unpacked extension..."
* Select cloned repository directory

# License

Toggle Declined Events for Google Calendar
Copyright (C) 2016 Jason Schmidt

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

# TO DO

* Promotional tile images
