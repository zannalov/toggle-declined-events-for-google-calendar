/*
Toggle Declined Events for Google Calendar
Copyright (C) 2016 Jason Schmidt

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

(function() {
    var CONSTANTS = {
        TOGGLE_BUTTON_ID: 'toggle-declined-events',
        STATE_UNKNOWN_TEXT: chrome.i18n.getMessage('buttonTextStateUnknown'),
        STATE_DECLINED_SHOWN: chrome.i18n.getMessage('buttonTextStateDeclinedShown'),
        STATE_DECLINED_HIDDEN: chrome.i18n.getMessage('buttonTextStateDeclinedHidden'),

        // As of 2016-09-21 18:31:41
        LOADING_BANNER_ID: 'lo-c',
        MORE_MENU_BUTTON_ID: 'mg-more',
        SETTINGS_SCREEN_HASH: '#settings-general_11',
        SETTINGS_SCREEN_ID: 'settingsForm',
        SETTINGS_CANCEL_BUTTON_ID: 'settings_cancel_btn',
        SETTINGS_SAVE_BUTTON_ID: 'settings_save_btn'
    };

    // Listen for all changes to the page
    var domEvents = [
        'DOMSubtreeModified',
        'DOMNodeInserted',
        'DOMNodeRemoved',
        'DOMCharacterDataModified',
        'DOMAttrModified'
    ];
    domEvents.forEach( function( domEvent ) {
        window.addEventListener( domEvent , main , true );
    } );

    // Current value of the setting (null === unknown)
    var showDeclined = null;

    // Main loop - executed when the DOM changes or the user interacts
    var goal = null;
    var extensionOpenedSettings = false;
    var debounceMainTimeoutHandle = null;
    function main() {
        if( null === debounceMainTimeoutHandle ) {
            debounceMainTimeoutHandle = setTimeout( _main , 1 );
        }
    }
    function _main() {
        debounceMainTimeoutHandle = null;
        try {
            // If something is loading, wait until it's finished (hiding the
            // loading message will trigger main again)
            if( isVisible( document.getElementById( CONSTANTS.LOADING_BANNER_ID ) ) ) {
                return;
            }

            // Used by button creation and intent code, so cache here
            var toggleButton = document.getElementById( CONSTANTS.TOGGLE_BUTTON_ID );

            // If More menu visible but toggle button isn't on the DOM
            var moreMenuButton = document.getElementById( CONSTANTS.MORE_MENU_BUTTON_ID );
            if( isVisible( moreMenuButton ) && !toggleButton ) {
                var s = '';
                s += '<div class="goog-inline-block goog-imageless-button' + ( null === showDeclined ? ' goog-imageless-button-disabled' : '' ) + '" role="button" aria-disabled="' + ( null === showDeclined ? 'true' : 'false' ) + '" aria-haspopup="true" data-tooltip="Toggle Declined Events - Chrome Extension" aria-label="Toggle Declined Events - Chrome Extension" style="-webkit-user-select: none;" id="' + CONSTANTS.TOGGLE_BUTTON_ID + '">';
                    s += '<div class="goog-inline-block goog-imageless-button-outer-box">';
                        s += '<div class="goog-inline-block goog-imageless-button-inner-box">';
                            s += '<div class="goog-imageless-button-pos">';
                                s += '<div class="goog-imageless-button-top-shadow">&nbsp;</div>';
                                if( null === showDeclined ) {
                                    s += '<div class="goog-imageless-button-content">' + CONSTANTS.STATE_UNKNOWN_TEXT + '</div>';
                                } else if( showDeclined ) {
                                    s += '<div class="goog-imageless-button-content">' + CONSTANTS.STATE_DECLINED_SHOWN + '</div>';
                                } else {
                                    s += '<div class="goog-imageless-button-content">' + CONSTANTS.STATE_DECLINED_HIDDEN + '</div>';
                                }
                            s += '</div>';
                        s += '</div>';
                    s += '</div>';
                s += '</div>';
                var container = document.createElement( 'div' );
                container.innerHTML = s;
                toggleButton = container.firstChild;

                moreMenuButton.parentNode.insertBefore( toggleButton , moreMenuButton );

                toggleButton.addEventListener( 'click' , function() {
                    setGoal( 'toggle' );
                } );

                return;
            }

            // If we have a goal, then we'll need to reach the settings page
            if( goal ) {
                // If it's not visible...
                var settingsScreenVisible = isVisible( document.getElementById( CONSTANTS.SETTINGS_SCREEN_ID ) );
                if( !settingsScreenVisible && CONSTANTS.SETTINGS_SCREEN_HASH !== window.location.hash ) {
                    extensionOpenedSettings = true;
                    window.location.hash = CONSTANTS.SETTINGS_SCREEN_HASH;
                    return;
                }
            }

            // Wait until the setting elements (the showDeclined toggle radio
            // buttons) are visible and one has been checked (meaning that the
            // settings have been loaded from the server)
            var settingsElements = document.getElementsByName( 'showDeclined' );
            if( settingsElements && 2 === settingsElements.length && isVisible( settingsElements[0] ) && isVisible( settingsElements[1] ) && ( settingsElements[0].checked || settingsElements[1].checked ) ) {
                // Ensure that we pick up changes in the value
                settingsElements[0].removeEventListener( 'change' , main );
                settingsElements[1].removeEventListener( 'change' , main );
                settingsElements[0].addEventListener( 'change' , main );
                settingsElements[1].addEventListener( 'change' , main );

                // If our intent was to toggle the value, do so
                if( 'toggle' === goal ) {
                    if( settingsElements[0].checked ) {
                        settingsElements[1].checked = true;
                    } else {
                        settingsElements[0].checked = true;
                    }
                }

                // Get the value from the "true" radio button
                var newShowDeclined = null;
                if( 'true' === settingsElements[0].attributes.value.value ) {
                    newShowDeclined = settingsElements[0].checked;
                } else {
                    newShowDeclined = settingsElements[1].checked;
                }

                // If the value has changed (or we first receive it)
                if( newShowDeclined !== showDeclined ) {
                    // Store the value
                    showDeclined = newShowDeclined;

                    // If the button has already been initialized
                    if( toggleButton ) {
                        // Update the button text
                        toggleButton.firstChild.firstChild.firstChild.childNodes[1].innerText = ( showDeclined ? CONSTANTS.STATE_DECLINED_SHOWN : CONSTANTS.STATE_DECLINED_HIDDEN );

                        // Remove the disabled class now that we know the setting
                        toggleButton.classList.remove( 'goog-imageless-button-disabled' );
                        toggleButton.attributes[ 'aria-disabled' ].value = 'false';
                    }
                }

                // If just fetching and we started on the calendar screen,
                // don't bother with save and return to calendar
                if( 'getState' === goal && extensionOpenedSettings ) {
                    extensionOpenedSettings = false;
                    simulateClick( document.getElementById( CONSTANTS.SETTINGS_CANCEL_BUTTON_ID ) );
                }

                // If changing the value, make sure it's saved, which takes us
                // back to the calendar
                if( 'toggle' === goal ) {
                    simulateClick( document.getElementById( CONSTANTS.SETTINGS_SAVE_BUTTON_ID ) );
                }

                // We've completed this intent
                goal = null;
            }
        } catch( e ) {
            // If anything goes wrong, don't crash, but do make sure it's logged
            console.error( e );
        }
    }

    // If the element exists, and exibits properties which indicate it's not
    // only attached to the DOM but visible to the user
    function isVisible( el ) {
        return Boolean( el && ( el.offsetWidth || el.offsetHeight || ( el.getClientRects && el.getClientRects().length ) ) );
    }

    // Click is composed of multiple steps, and simulating a click requires we
    // mimic all of those steps, lest something expecting one of them not be
    // fired.
    function simulateClick( el ) {
        // In the order they would occur
        var events = [
            'mouseover',
            'mousedown',
            'mouseup',
            'click',
            'mouseout'
        ];

        // For each event in order
        events.forEach( function( eventName ) {
            // Add the event to the end of the async queue
            setTimeout( function() {
                // Generate the DOM event
                var event = new MouseEvent( eventName , {
                    view: window,
                    bubbles: true,
                    cancelable: true
                } );

                // Dispatch it on th element
                el.dispatchEvent( event );
            } , 1 );
        } );

        // Add a call to main to the end of the queue, just in case
        setTimeout( main , 1 );
    }

    // Set the goal of the main loop and kick it off
    function setGoal( intent ) {
        goal = intent;
        main();
    }

    // Initialize by trying to get the current state
    setGoal( 'getState' );
})();
