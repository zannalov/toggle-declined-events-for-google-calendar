#!/bin/sh
exec zip toggle-declined-events-for-google-calendar.zip \
    LICENSE.txt \
    _locales/*/messages.json \
    content_scripts/toggle-declined-events-for-google-calendar.css \
    content_scripts/toggle-declined-events-for-google-calendar.js \
    icons/128.png \
    manifest.json \
    # end
